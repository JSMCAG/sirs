package presentation.ws.handler;

import java.io.IOException;
import java.security.InvalidKeyException;
import java.util.Set;

import javax.xml.namespace.QName;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPMessage;
import javax.xml.transform.TransformerException;
import javax.xml.ws.handler.MessageContext;
import javax.xml.ws.handler.soap.SOAPHandler;
import javax.xml.ws.handler.soap.SOAPMessageContext;

import org.xml.sax.SAXException;

/**
 * This SOAPHandler shows how to set/get values from headers in inbound/outbound SOAP
 * messages. A header is created in an outbound message and is read on an inbound message.
 */
public class ClientHandler
        implements SOAPHandler<SOAPMessageContext> {

    //
    // Handler interface methods
    //
    /*
     * (non-Javadoc)
     * 
     * @see javax.xml.ws.handler.soap.SOAPHandler#getHeaders()
     */
    public Set<QName> getHeaders() {
        return null;
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * javax.xml.ws.handler.Handler#handleMessage(javax.xml.ws.handler.MessageContext)
     */
    // TODO Verificar se as chaves estam correctas     
    public boolean handleMessage(SOAPMessageContext smc) {
        System.out.println("ClientHandler: Handling message.");

        Boolean outboundElement = (Boolean) smc.get(MessageContext.MESSAGE_OUTBOUND_PROPERTY);

        try {//servidor
            if (outboundElement.booleanValue()) {
                return handleOutboundMessage(smc);
            } else {
                return handleInboundMessage(smc);
            }
        } catch (Exception e) {
            System.out.printf("Caught exception in handleMessage: %s%n", e);
            return false;
        }
    }

    /**
     * @param smc
     * @throws SOAPException
     * @throws InvalidKeyException
     * @throws IOException
     * @throws ParserConfigurationException
     * @throws SAXException
     * @throws TransformerException
     */
    private boolean handleInboundMessage(SOAPMessageContext smc) throws SOAPException,
            InvalidKeyException,
            IOException,
            TransformerException,
            SAXException,
            ParserConfigurationException {
        /* //entrada
         //        System.out.println("Message is inbound SOAP message...");

         // get SOAP envelope header
         SOAPMessage msg = smc.getMessage();
         SOAPPart sp = msg.getSOAPPart();
         SOAPEnvelope se = sp.getEnvelope();

         //        printTeste(msg, "inicio");

         //Se o corpo nao estiver encriptado e a funcao nao for a de listar entao ele nao aceita a mensagem.
         if (!SoapSecurityService.isEncrypted(se)) {
             //            System.out.println("Recieved PlainText Message");
             final String function = SoapUtilities.getFunctionName(se);
             return "listarResponse".equals(function);
         }

         //        System.out.println("Recieved Encrypted Message");
         //        msg.writeTo(System.out);
         //        System.out.println();

         ActualContext actualContext = ActualContext.getInstance();
         PrivateKey privateKey = actualContext.getLoggedUserKeys().getPrivate();

         SoapSecurityService.decryptBody(se, privateKey);
         //        System.out.println("Decrypted Body");
         //        msg.writeTo(System.out);
         //        System.out.println();

         PublicKey senderPublicKey = actualContext.getReceiverPublicKey();

         Boolean correct = SoapSecurityService.handleDigitalSignature(se, senderPublicKey);
         //        System.out.println("Verified Digital Signature");
         //        msg.writeTo(System.out);
         //        System.out.println();
        */
        //printMessageContext(smc);
        return true;//correct;
    }

    /**
     * @param smc
     * @throws SOAPException
     * @throws IOException
     * @throws TransformerException
     * @throws InvalidKeyException
     */
    private boolean handleOutboundMessage(SOAPMessageContext smc) throws SOAPException,
            IOException,
            TransformerException,
            InvalidKeyException {
        /*   //saida 
           //        System.out.println("Message is outbound SOAP message...");

           // get SOAP envelope
           SOAPMessage msg = smc.getMessage();
           SOAPPart sp = msg.getSOAPPart();
           SOAPEnvelope se = sp.getEnvelope();

           String funcao = SoapUtilities.getFunctionName(se);
           if ("listar".equals(funcao)) {
               //            System.out.println("Message needs no Encryption");
               return true;
           }

           //        System.out.println("Message to Encrypt");
           //        msg.writeTo(System.out);
           //        System.out.println();

           ActualContext actualContext = ActualContext.getInstance();
           PrivateKey privateKey = actualContext.getLoggedUserKeys().getPrivate();

           SoapSecurityService.generateDigitalSignature(se, privateKey);
           //        System.out.println("Generated DigitalSignature");
           //        msg.writeTo(System.out);
           //        System.out.println();

           PublicKey receiverPublicKey = actualContext.getReceiverPublicKey();

           SoapSecurityService.encryptBody(se, receiverPublicKey);
           //        System.out.println("Encrypted Body");
           //        msg.writeTo(System.out);
           //        System.out.println();

        */
        printMessageContext(smc);
        return true;
    }

    /**
     * @param msg
     * @param teste
     * @throws SOAPException
     * @throws IOException
     */
    private void printTeste(SOAPMessage msg, String teste) throws SOAPException, IOException {
        System.out.println(teste);
        msg.writeTo(System.out);
        System.out.println();
    }

    /*
     * (non-Javadoc)
     * 
     * @see javax.xml.ws.handler.Handler#handleFault(javax.xml.ws.handler.MessageContext)
     */
    @Override
    public boolean handleFault(SOAPMessageContext smc) {
        return true;
    }

    /*
     * (non-Javadoc)
     * 
     * @see javax.xml.ws.handler.Handler#close(javax.xml.ws.handler.MessageContext)
     */
    @Override
    public void close(MessageContext messageContext) {
        //System.out.println("close");
    }

    private void printMessageContext(MessageContext map) {
        System.out.println("Message context: (scope,key,value)");
        try {
            java.util.Iterator it = map.keySet().iterator();
            while (it.hasNext()) {
                Object key = it.next();
                Object value = map.get(key);

                String keyString;
                if (key == null)
                    keyString = "null";
                else
                    keyString = key.toString();

                String valueString;
                if (value == null)
                    valueString = "null";
                else
                    valueString = value.toString();

                Object scope = map.getScope(keyString);
                String scopeString;
                if (scope == null)
                    scopeString = "null";
                else
                    scopeString = scope.toString();
                scopeString = scopeString.toLowerCase();

                System.out.println("(" + scopeString + "," + keyString + "," + valueString + ")");
            }

        } catch (Exception e) {
            System.out.printf("Exception while printing message context: %s%n", e);
        }
    }



}
