package service.dto;

import java.io.Serializable;

public class ValidatedDocumentDto
        implements Serializable {
    /**
     * 
     */
    private static final long serialVersionUID = 6981609128974959722L;
    /**
     * 
     */
    public final String timestamp;
    public final String tSASignature;



    public ValidatedDocumentDto(String timestamp, String signature) {
        this.timestamp = timestamp;
        this.tSASignature = signature;
    }

    /**
     * @return the timestamp
     */
    public String getTimestamp() {
        return timestamp;
    }

    /**
     * @return the tSASignature
     */
    public String getTSASignature() {
        return tSASignature;
    }





}
