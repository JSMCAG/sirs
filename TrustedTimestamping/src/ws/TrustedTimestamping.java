package ws;

import javax.jws.WebMethod;
import javax.jws.WebService;

import service.dto.ValidatedDocumentDto;

@WebService
public interface TrustedTimestamping {

    @WebMethod
    ValidatedDocumentDto validateSignature(String name) throws Throwable;

}
