package ws;

import javax.jws.WebService;

import service.ValidateSignatureService;
import service.dto.ValidatedDocumentDto;

@WebService(endpointInterface = "ws.TrustedTimestampingImpl")
public class TrustedTimestampingImpl
        implements TrustedTimestamping {

    public ValidatedDocumentDto validateSignature(String name) throws Throwable {
        ValidateSignatureService s = new ValidateSignatureService(name);
        try {
            s.execute();
        } catch (Throwable e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            throw new Throwable("Não foi possivel validar assinatura");
        }
        return s.getResult();
    }

}
