package core;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

public class FileManager {

    public FileManager() {
        // TODO Auto-generated constructor stub
    }

    public static void writeFile(String path, String fileName, byte[] content) throws FileNotFoundException,
            IOException {
        File dirs = new File(path);
        dirs.mkdirs();
        writeFile(path + fileName, content);

    }

    public static void writeFile(String filePath, byte[] content) throws IOException {
        FileOutputStream fos = new FileOutputStream(filePath);
        fos.write(content);
        fos.close();
    }

    public static byte[] readFile(String path) throws FileNotFoundException, IOException {
        FileInputStream fis = new FileInputStream(path);
        byte[] content = new byte[fis.available()];
        fis.read(content);
        fis.close();
        return content;
    }

}
