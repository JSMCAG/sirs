/**
 * 
 */
package exception;

/**
 * @author Filipe Apolinário
 */
public class InvalidSignatureValidationException extends
        RuntimeException {

    /**
     * 
     */
    public InvalidSignatureValidationException() {
        super();
    }

    /**
     * @param arg0
     */
    public InvalidSignatureValidationException(String arg0) {
        super(arg0);
    }

    /**
     * @param arg0
     */
    public InvalidSignatureValidationException(Throwable arg0) {
        super(arg0);
    }

    /**
     * @param arg0
     * @param arg1
     */
    public InvalidSignatureValidationException(String arg0, Throwable arg1) {
        super(arg0, arg1);
    }

    /**
     * @param arg0
     * @param arg1
     * @param arg2
     * @param arg3
     */
    public InvalidSignatureValidationException(String arg0,
                                               Throwable arg1,
                                               boolean arg2,
                                               boolean arg3) {
        super(arg0, arg1, arg2, arg3);
    }

}
